#ifndef SOFT_ELECTRON_SELECTOR_HH
#define SOFT_ELECTRON_SELECTOR_HH

#include "xAODEgamma/ElectronContainerFwd.h"
#include "xAODJet/JetFwd.h"

#include "SoftElectronSelectorConfig.hh"

class SoftElectronSelector
{
public:
  SoftElectronSelector(SoftElectronSelectorConfig = SoftElectronSelectorConfig());
  typedef std::vector<const xAOD::Electron*> Electrons;
  Electrons get_electrons(const xAOD::Jet& jet, const xAOD::ElectronContainer& electrons) const;

private:
  typedef SG::AuxElement AE;

  bool passed_cuts(const xAOD::Jet& jet, const xAOD::Electron &el) const;
  SoftElectronSelectorConfig::Cuts m_electron_select_cfg;
};

#endif
