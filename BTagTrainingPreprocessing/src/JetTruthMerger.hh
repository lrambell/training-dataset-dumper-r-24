#ifndef JETTRUTHMERGER_HH
#define JETTRUTHMERGER_HH

#include "xAODJet/JetFwd.h"
#include "xAODBase/IParticleContainer.h"

#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"

#include <string>

class JetTruthMerger
{
public:
  typedef SG::AuxElement AE;
  typedef ElementLink<xAOD::IParticleContainer> PartLink;
  typedef std::vector<PartLink> PartLinks;

  JetTruthMerger(const std::vector<std::string>& incoming,
                 const std::string& outgoing);

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet) const;

private:
  std::vector<SG::AuxElement::ConstAccessor<PartLinks>> m_acc;
  SG::AuxElement::Decorator<PartLinks> m_deco;
};

#endif
