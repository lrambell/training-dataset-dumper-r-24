#ifndef B_JET_COPIER_HH
#define B_JET_COPIER_HH

#include "xAODJet/JetContainerFwd.h"
#include "xAODBTagging/BTaggingContainerFwd.h"
#include "xAODCore/ShallowAuxContainer.h"

#include <memory>

struct BJetShallowAux {
  std::unique_ptr<xAOD::ShallowAuxContainer> jetAux;
  std::unique_ptr<xAOD::BTaggingContainer> btag;
  std::unique_ptr<xAOD::ShallowAuxContainer> btagAux;
};

struct ShallowCopyLinkers;

class BJetShallowCopier {
public:
  using JCP = std::unique_ptr<xAOD::JetContainer>;
  using BJetCopy = std::pair<JCP, BJetShallowAux>;
  BJetShallowCopier(const std::string& btag_link_name);
  ~BJetShallowCopier();
  BJetCopy shallowCopyBJets(const xAOD::JetContainer& jets) const;
private:
  std::unique_ptr<ShallowCopyLinkers> m_links;
  bool m_copyBtagObject = true;
};

#endif
