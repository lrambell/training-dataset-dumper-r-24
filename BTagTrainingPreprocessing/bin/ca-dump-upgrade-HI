#!/usr/bin/env python

"""
This dumps datasets for upgrade studies

The primary difference between this and ca-dump-single-btag is that
upgrade is still working in release 21. To support release 21 AODs in
release 22, we have to augment the data in a few ways.
"""

from BTagTrainingPreprocessing import dumper

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from AthenaConfiguration.ComponentFactory import CompFactory
from GaudiKernel.Configurable import DEBUG, INFO

from argparse import ArgumentParser
from itertools import chain
import sys

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='output.h5')
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-d','--debug', action='store_true')
    return parser.parse_args()


def run():
    args = get_args()

    cfgFlags.Input.Files = list(
        chain.from_iterable(f.split(',') for f in args.input_files))

    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    cfgFlags.Exec.OutputLevel = DEBUG if args.debug else INFO

    cfgFlags.lock()

    ca = getConfig(cfgFlags)

    ca.merge(PoolReadCfg(cfgFlags))

    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        'PoorMansIpAugmenterAlg', prefix='btagIp_'))

    btag_name = 'BTagging_AntiKt4HI'
    jet_name = 'AntiKt4HIJets'
    ca.addEventAlgo(CompFactory.BTagToJetLinkerAlg(
        'jetToBTag',
        newLink=f'{btag_name}.jetLink',
        oldLink=f'{jet_name}.btaggingLink'
    ))

    ca.merge(dumper.getDumperConfig(args))

    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)



