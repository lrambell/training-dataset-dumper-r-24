#!/usr/bin/env python

"""
Demonstration for minimal b-tagging

This tags jets in two ways:

- By creating a new BTagging object, and then associating tracks and
  running a tagger.

- Directly on the jet. No BTagging object is created, all the
  variables are added to the jet directly.

Unlike most code running in this framework, we avoid running DL2 in
the dataset dumper itself. This is just to provide an example: if
you're trying to use this code to study new NN trainings we recommend
configuring DL2 as part of the dataset dumper json configuration.
"""

import sys

from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg,
)

from BTagTrainingPreprocessing import dumper


def getSimpleBTagConfig(flags, nnFile):
    """
    This example builds a BTagging object, and associates tracks. The
    tagging is left for the dumper code here.
    """
    ca = ComponentAccumulator()

    btag_name = 'TestBTagging'
    jet_name = 'AntiKt4EMPFlowJets'
    track_name = "InDetTrackParticles"
    builder = CompFactory.FlavorTagDiscriminants.BTaggingBuilderAlg(
        'btagbuilder',
        jetName=jet_name,
        btaggingName=btag_name)
    ca.addEventAlgo(builder)

    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        'PoorMansIpAugmenterAlg',
        trackContainer=track_name,
    ))

    ca.addEventAlgo(CompFactory.JetToBTagLinkerAlg(
        'jetToBtag',
        oldLink=f'{btag_name}.jetLink',
        newLink=f'{jet_name}.newBtaggingLink'
    ))

    track_decor = "TracksForMinimalTag"
    ca.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=jet_name,
        InputParticleCollection=track_name,
        OutputParticleDecoration=track_decor
    ))

    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.BTagTrackLinkCopyAlg(
            'copy_alg',
            jetTracks=f'{jet_name}.{track_decor}',
            btagTracks=f'{btag_name}.BTagTrackToJetAssociator',
            jetLinkName=f'{btag_name}.jetLink'
        )
    )

    # Now we have to add an algorithm that tags the jets with dips
    # The input and output remapping is handled via a map in DL2.
    #
    # The file above adds dipsLoose20210517_p*, we'll call them
    # dips_p* on the btag object.
    variableRemapping = {
        **{f'dipsLoose20210517_p{x}': f'dips_p{x}' for x in 'cub'},
        'btagIp_': 'poboyIp_',
    }
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.BTagDecoratorAlg(
            'simpleBTagAlg',
            container=btag_name,
            constituentContainer=track_name,
            decorator=CompFactory.FlavorTagDiscriminants.DL2Tool(
                'simpleDips',
                nnFile=nnFile,
                variableRemapping=variableRemapping,
                # note that the tracks are associated to BTagging as
                # a TrackParticle container.
                trackLinkType='TRACK_PARTICLE',
            ),
        )
    )
    return ca

def getSimpleJetTagConfig(flags, nnFile):
    """
    This example tags jets directly: there is no intermediate
    b-tagging object
    """

    ca = ComponentAccumulator()

    # first add the track augmentation to define peragee coordinates
    jet_name = 'AntiKt4EMPFlowJets'
    trackContainer = 'InDetTrackParticles'
    primaryVertexContainer = 'PrimaryVertices'
    simpleTrackIpPrefix = 'simpleIp_'
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            'SimpleTrackAugmenter',
            trackContainer=trackContainer,
            primaryVertexContainer=primaryVertexContainer,
            prefix=simpleTrackIpPrefix,
        )
    )

    # now we assicoate the tracks to the jet
    tracksOnJetDecoratorName = "TracksForMinimalJetTag"
    ca.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=jet_name,
        InputParticleCollection=trackContainer,
        OutputParticleDecoration=tracksOnJetDecoratorName,
        MinimumJetPt=15e3,
    ))

    # Now we have to add an algorithm that tags the jets with dips
    # The input and output remapping is handled via a map in DL2.
    #
    # The file above adds dipsLoose20210517_p*, we'll call them
    # dips_p* on the jet.
    variableRemapping = {
        'BTagTrackToJetAssociator': tracksOnJetDecoratorName,
        **{f'dipsLoose20210517_p{x}': f'dipsOnJet_p{x}' for x in 'cub'},
        'btagIp_': simpleTrackIpPrefix,
    }
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.JetTagDecoratorAlg(
            'simpleJetTagAlg',
            container=jet_name,
            constituentContainer=trackContainer,
            decorator=CompFactory.FlavorTagDiscriminants.DL2Tool(
                'simpleDipsToJet',
                nnFile=nnFile,
                variableRemapping=variableRemapping,
                # note that the tracks are associated to the jet as
                # and IParticle container.
                trackLinkType='IPARTICLE',
            ),
        )
    )
    return ca

def getSingleBTagConfig(flags):

    ca = ComponentAccumulator()

    nnFile = 'dev/BTagging/20210517/dipsLoose/antikt4empflow/network.json'

    ca.merge(getSimpleBTagConfig(flags, nnFile))
    ca.merge(getSimpleJetTagConfig(flags, nnFile))

    return ca


def run():
    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.merge(PoolReadCfg(cfgFlags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # add minimal b-tagging
    ca.merge(getSingleBTagConfig(cfgFlags))

    # add dumper
    ca.merge(dumper.getDumperConfig(args))

    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
